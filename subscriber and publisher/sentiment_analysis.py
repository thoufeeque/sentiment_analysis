#!/usr/bin/env python

#copy write (c) 2018,willow grage, Inc.
#All rights reserved 
#Copyright <2018> <thoufeeque s>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#python 2.x program for sentiment analysis.
#topic:text  


import rospy
from std_msgs.msg import String

#assign possible words to lists
positive_words = [
		 "good morning","beautifull",
		 "fantastic","amazing"
		 ]
negative_words = [
		"failure","bad",
		"upset","unhappy"
		 ]
neutral_words = [
		"he","it",
		"house","man"	
		]  

#function for read the text from publisher
def callback(data):
	
        d=data.data
#analysing the recieved word 
        for i in range(0,4):

    		if d==positive_words[i]:
			hello_str="it is positive word"
			rospy.loginfo(hello_str)
    		if d==negative_words[i]:
			hello_str="it is negative word"
			rospy.loginfo(hello_str)
    		if d==neutral_words[i]:
			hello_str="it is neutral word"
			rospy.loginfo(hello_str)


def listener():
    
	# In ROS, nodes are uniquely named. If two nodes with the same
        # name are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'sentiment_analysis' node so that multiple listeners can
        # run simultaneously.
#node will take on the name sentiment_analysis.
	rospy.init_node('sentment_analysis', anonymous=True)
#declares that the node subscribes to the text topic which is of type std_msgs.msgs.String.
	rospy.Subscriber('text', String, callback)

# spin() simply keeps python from exiting until this node is stopped
        rospy.spin()

#main fuction
if __name__ == '__main__':
	listener()
