        SENTIMENTAL ANALYSIS WITH ROS SUBSCRIBER AND PUBLISHER


 PROJECT DESCRIPTION
:The project deals with the recognition of speech using speech recognition and
convert it into text then analysis whether the text is positive, negative or
neutral.
For installing ubuntu :https://www.ubuntu.com/download/desktop
 
➢ ROS
ROS (Robot Operating System) provides libraries and tools to help software
developers create robot applications. It provides hardware abstraction, device
drivers, libraries, visualizers, messagepassing, package management, and
more. ROS is licensed under an open source, BSD license.

* Install ROS kinetic
ROS Kinetic only supports Wily (Ubuntu 15.10), Xenial (Ubuntu 16.04) and
Jessie (Debian 8) for debian packages. Follow the steps in the link fpr installing
ros kinetic.
http://wiki.ros.org/kinetic/Installation/Ubuntu

 ➢ Speech recognition
Speech recognition develops methodologies and technologies that enables the
recognition and translation of spoken language into text by computers. It is
also known as "automatic speech recognition" (ASR), "computer speech
recognition", or just "speech to text" (STT).

 Requirements
To use all of the functionality of the library, you should have:
 
 ➢ Python 2.6, 2.7, or 3.3+ (required)
 ➢ PyAudio 0.2.11+ (required only if you need to use microphone
input,
Microphone)
The installation instructions on the PyAudio website are quite good for
convenience, they are summarized below:

Install the latest release using Pip: execute
:sudo aptget install portaudio19dev pythonalldev python3alldev &&
sudo pip install pyaudio (replace pip with pip3 if using Python 3).Then use the command for installing speech
recognition :pip install SpeechRecognition.

If you don’t install the pip then you can move on and install Pip onto your
Ubuntu VPS. The installation of Pip is very simple, and is done through “apt-
get”. The only thing you need to do is to run the following command: sudo apt-
get install pythonpip
   To quickly try the speech recognition, run: python m speech_recognition after
installing.

 ➢ Create a work space for catkin
This tutorial covers how to set up acatkin workspace in which one or more
catkin packages can be built.
Link: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

 ➢ Create ros package
This covers using roscreate pkg or catkin to create anew package.
Link:http://wiki.ros.org/ROS/Tutorials/CreatingPackage

 ➢ Writing publisher and subscriber
This tutorial covers how to write a publisher and subscriber node in python.
Link :http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber(pytho
n)
After writing publisher and subscriber changes are needed in the
program speech.py and sentiment_analysis.py for recognising the audio
and analysing the audio wheather it is poitive,negative or neutral.

 ➢ Examining the program
Link:http://wiki.ros.org/ROS/Tutorials/ExaminingPublisherSubscriber