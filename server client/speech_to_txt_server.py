#!/usr/bin/env python

#copy write (c) 2018,willow grage, Inc.
#All rights reserved 
#Copyright <2018> <thoufeeque s>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#python 2.x program for sentiment analysis.
from sentiment_analysis_2.srv import *
import rospy

def handle_analysis(req):
#assign possible words to lists 
        positive_words = [
		 "good morning","beautifull",
		 "fantastic","amazing"
		 ]
	negative_words = [
		"failure","upset",
		"upset","unhappy"
		 ]
	neutral_words = [
		"he","it",
		"house","man"	
		]
#getting data from client
    	x=req.a
    	print "converted text is ",req.a
    	print "Returning"

#analysing the recieved word
        for i in range(0,4):
           if x==positive_words[i]:
           	b='It is a positive word'
                
	   if x==negative_words[i]:
		b='It is a negative word'
              	
	   if x==neutral_words[i]:
		b='It is a neutral word'
                
#returning the result back to client
           return SentimentResponse(b)
#creating server node
def Sentiment_server():
    rospy.init_node('Sentiment_server')
    s = rospy.Service('analysis', Sentiment, handle_analysis)
    print "Ready to analize."
    rospy.spin()
#start from main
if __name__ == "__main__":
    Sentiment_server()
